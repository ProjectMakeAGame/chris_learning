﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// TODO: Implement Game states so we know when enimies should spawn and when players can 
/// create towers.
/// </summary>
public abstract class GameState {

    public abstract void Handle();

}
