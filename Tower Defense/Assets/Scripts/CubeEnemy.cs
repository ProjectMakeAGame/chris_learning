﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CubeEnemy : MonoBehaviour
{
    GameObject _tileMap;
    TileMap tileMap;
    public List<int[]> path = new List<int[]>();
    public int speed = 1;
    private int[] nextDestination = new int[3];
    Vector3 target = new Vector3();
    int numberOfSpaces = 0;
    int pathLength;

    // Use this for initialization
    void Start()
    {
  
    }


    /// <summary>
    /// This is called once right when the object enters the game
    /// </summary>
    void Awake()
    {

        _tileMap = GameObject.FindGameObjectWithTag("Map");
        tileMap = _tileMap.GetComponent<TileMap>();
        path = tileMap.FindShortestPathThroughMaze();
        pathLength = path.ToArray().Length;
        nextDestination[0] = (int)path[numberOfSpaces + 1].GetValue(0);
        nextDestination[1] = (int)path[numberOfSpaces + 1].GetValue(1);
        Debug.Log(nextDestination[0]);

        //foreach (int[] number in path){
        //    Debug.Log("In the Path: "  + number[0] + " , " +  number[1]);
        //}

        target.x = nextDestination[0] + 0.5f;
        target.z = nextDestination[1] + 0.5f;
        target.y = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {


    }

    void FixedUpdate()
    {
        if(target.x == transform.position.x && target.z == transform.position.z)
        {
            Debug.Log("Current Pos: " + numberOfSpaces);
            if (numberOfSpaces == pathLength)
            {
                CubeCompleated();
            }
            else
            {
                numberOfSpaces++;
                nextDestination[0] = (int)path[numberOfSpaces].GetValue(0);
                nextDestination[1] = (int)path[numberOfSpaces].GetValue(1);
                target.x = nextDestination[0] + 0.5f;
                target.z = nextDestination[1] + 0.5f;
                Debug.Log("New Targert: " + target.x + " , " + target.z);
            }
        }
        


        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        //transform.position += Vector3.right * speed * Time.deltaTime;

    }

    private void CubeCompleated()
    {
        Destroy(gameObject);
    }

    
}
