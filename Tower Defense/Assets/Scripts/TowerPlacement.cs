﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerPlacement : MonoBehaviour {

    private TileMapMouse _mouse;
    private TileMap _tileMap;
    Vector3 currentMousePosition;
    public GameObject Tower;

    // Use this for initialization
    void Start () {
        _mouse = GetComponent<TileMapMouse>();
        _tileMap = GetComponentInParent<TileMap>();
	}
	
	// Update is called once per frame
	void Update () {
        currentMousePosition = _mouse.currentTileCoord;
        if (Input.GetMouseButtonDown(1))
        {
            PlaceTower();   
        }
	}

    private void PlaceTower()
    {
        //We want to place towers in the centers of squars
        Vector3 towerPlacementPotstion = new Vector3(currentMousePosition.x + 0.5f, currentMousePosition.y, currentMousePosition.z + 0.5f);
        Instantiate(Tower, towerPlacementPotstion, _mouse.transform.rotation);
        _tileMap.UpdateMaze((int)currentMousePosition.x, (int)currentMousePosition.z, true);
        Debug.Log("TowerPlacedAt: " + currentMousePosition.x + " , " + currentMousePosition.z);
    }
}
