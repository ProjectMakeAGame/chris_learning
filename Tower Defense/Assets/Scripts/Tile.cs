﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile {

    public readonly int height;
    public readonly int width;


	public Tile(int height, int width)
    {
        this.height = height;
        this.width = width;
    }


    /// <summary>
    /// Returns the Realworld position of the tile
    /// </summary>
    /// <returns></returns>
    public Vector3 Position()
    {
        float height = 1f;
        float width = 1f;

        return new Vector3(
            height * this.height,
            0,
            width * this.width
        );
    }
}
