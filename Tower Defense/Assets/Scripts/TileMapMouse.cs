﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMapMouse : MonoBehaviour {

    public TileMap tileMap;
    public Vector3 currentTileCoord;
    public Transform selectionCube;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (tileMap.GetComponent<Collider>().Raycast(ray, out hitInfo, Mathf.Infinity)){
            float x = Mathf.FloorToInt((hitInfo.point.x) / tileMap.tileSize);
            float z = Mathf.FloorToInt((hitInfo.point.z) / tileMap.tileSize);
            currentTileCoord.x = x;
            currentTileCoord.z = z;
            selectionCube.transform.position = currentTileCoord + SelectionCubeoffset();
        }
        else
        {

        }
	}

    Vector3 SelectionCubeoffset()
    {
        return selectionCube.transform.localScale /2 ;
    }
}
