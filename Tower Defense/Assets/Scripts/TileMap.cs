using UnityEngine;
using System.Collections.Generic;
using AIProject;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class TileMap : MonoBehaviour {

    public static int size = 10;
    public float tileSize = 1.0f;
    public GameObject enemySpawn;
    public GameObject enemyDestination;
    public GameObject[] mapTile;
    private Maze myMaze;
    public List<int[]> path = new List<int[]>();

    //Holds the map that shows space enimies can travel
    /// 'o' is an open space
    /// 'x' is a closed space
    private char[,] maze = new char[size, size];

	// Use this for initialization
	void Start () {
        BuildMesh();
        InitializeMaze();
        //GenerateMap();

    }
	
	public void BuildMesh() {
		
		int numTiles = size * size;
		int numTris = numTiles * 2;
		
		int vsize_x = size +1;
		int vsize_z = size + 1;
		int numVerts = vsize_x * vsize_z;
		
		// Generate the mesh data
		Vector3[] vertices = new Vector3[ numVerts ];
		Vector3[] normals = new Vector3[numVerts];
		Vector2[] uv = new Vector2[numVerts];
		int[] triangles = new int[ numTris * 3 ];

		int x, z;
		for(z=0; z < vsize_z; z++) {
			for(x=0; x < vsize_x; x++) {
				vertices[ z * vsize_x + x ] = new Vector3( x*tileSize, 0, z*tileSize );
				normals[ z * vsize_x + x ] = Vector3.up;
				uv[ z * vsize_x + x ] = new Vector2( (float)x / vsize_x, (float)z / vsize_z );
			}
		}
		Debug.Log ("Done Verts!");
		
		for(z=0; z < size; z++) {
			for(x=0; x < size; x++) {
				int squareIndex = z * size + x;
				int triOffset = squareIndex * 6;
				triangles[triOffset + 0] = z * vsize_x + x + 		   0;
				triangles[triOffset + 1] = z * vsize_x + x + vsize_x + 0;
				triangles[triOffset + 2] = z * vsize_x + x + vsize_x + 1;
				
				triangles[triOffset + 3] = z * vsize_x + x + 		   0;
				triangles[triOffset + 4] = z * vsize_x + x + vsize_x + 1;
				triangles[triOffset + 5] = z * vsize_x + x + 		   1;
			}
		}
		
		Debug.Log ("Done Triangles!");
		
		// Create a new Mesh and populate with the data
		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.normals = normals;
		mesh.uv = uv;
		
		// Assign our mesh to our filter/renderer/collider
		MeshFilter mesh_filter = GetComponent<MeshFilter>();
		MeshRenderer mesh_renderer = GetComponent<MeshRenderer>();
		MeshCollider mesh_collider = GetComponent<MeshCollider>();
		
		mesh_filter.mesh = mesh;
		mesh_collider.sharedMesh = mesh;
		Debug.Log ("Done Mesh!");
		
	}

    public void GenerateMap()
    {
        for (int width = 0; width < size; width++)
        {
            for (int hight = 0; hight < size; hight++)
            {
                //Instantiate Map Tile
                int randomTileIndex = Random.Range(0, mapTile.Length);
                Tile tile = new Tile(hight , width);

                //GameObject tileGO = (GameObject)
                Instantiate(mapTile[randomTileIndex], tile.Position(), Quaternion.identity, this.transform);
                //MeshRenderer mr = tileGO.GetComponentInChildren<MeshRenderer>();
            }
        }
    }


    /// <summary>
    /// Initializing the Maze
    /// 'o' is an open space
    /// 'x' is a closed space
    /// </summary>
    private void InitializeMaze()
    {
        int spawnX = (int)enemySpawn.transform.position.x - 1;
        int spawnY = (int)enemySpawn.transform.position.z - 1;
        int destX = (int)enemyDestination.transform.position.x - 1;
        int destY = (int)enemyDestination.transform.position.z - 1;
        myMaze = new Maze(size, spawnX, spawnY, destX, destY);
        for (int i = 0; i < size -1; i++)
        {
            for(int j = 0; j < size -1 ; j ++)
            {
                maze[i, j] = 'o';
            }
        }
        myMaze.MakeMaze(maze);
    }


    /// <summary>
    /// This Mehtod is called to add or deleate an
    /// object from the Maze
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="ObjectAdded">True adds object, False Deleates</param>
    public void UpdateMaze(int x, int y, bool ObjectAdded)
    {
        if (ObjectAdded)
        {
            maze[x, y] = 'x';
        }
        else
        {
            maze[x, y] = 'o';
        }
        
        myMaze.MakeMaze(maze);
    }

    /// <summary>
    /// This method finds an returns the shortest
    /// path through the maze.
    /// </summary>
    public List<int[]> FindShortestPathThroughMaze()
    {
        myMaze.path.Clear();
        path.Clear();
        int spawnX = (int)enemySpawn.transform.position.x -1;
        int spawnY = (int)enemySpawn.transform.position.z -1 ;
        int destX = (int)enemyDestination.transform.position.x - 1;
        int destY = (int)enemyDestination.transform.position.z -1;
        Debug.Log("Maze Size" + size);
        Debug.Log("Spawn: " + spawnX + " , " + spawnY);
        Debug.Log("Destination: " + destX + " , " + destY);
        int res = myMaze.FindShortestPath();
        Debug.Log("Found Path: " + res);
        if (myMaze.path.Count > 0)
        {
            int i = 0;
            foreach (Cell c in myMaze.path)
            {
                int[] cellContents = new int[3];
                cellContents[0] = c.Row;
                cellContents[1] = c.Column;
                cellContents[2] = c.Distance;
                
                path.Add(cellContents);
                i++;
                Debug.Log(cellContents[1]);
                Debug.Log(c);
            }

        }

        path.Reverse();
        return path;
    }


}
