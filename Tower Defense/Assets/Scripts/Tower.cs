﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

    public float damageDealt;
    public float timeBetweenDamage;
    public float range;
    private float timer;
    public GameObject Target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        Target = FindTarget();
        try
        {
            var distance = Vector3.Distance(Target.transform.position, this.transform.position);
            if (distance <= range && timer >= timeBetweenDamage)
            {
                DamageTarget();
            }
        }
        catch
        {

        }
	}

    private void DamageTarget()
    {
        timer = 0f;
        EnemyHealth TargetCube = Target.GetComponent<EnemyHealth>();
        TargetCube.TakeDamage(damageDealt);
    }

    private GameObject FindTarget()
    {
        GameObject[] objectsWithTag = GameObject.FindGameObjectsWithTag("Enemy");
        int numberOfGameObjects = objectsWithTag.Length;
        GameObject closestObject = null;
        for (int i = 0; i < numberOfGameObjects; i ++)
        {
            if (closestObject == null)
            {
                print("Found a close object");
                closestObject = objectsWithTag[i];
            }
            //compares distances
            if (Vector3.Distance(transform.position, objectsWithTag[i].transform.position) <= Vector3.Distance(transform.position, closestObject.transform.position))
            {
                closestObject = objectsWithTag[i];
            }
        }
        return closestObject;
    }


    private bool IsTargetDead()
    {
        //Maybe we also need to be checking if the target exists
        EnemyHealth TargetCube = Target.GetComponent<EnemyHealth>();
        
        if(TargetCube.currentHealth <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
