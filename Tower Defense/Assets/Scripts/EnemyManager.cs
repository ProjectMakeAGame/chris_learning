﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    public TileMap tileMap;
    public bool GameOver;
    public float spawnTime;
    public Transform spawnPoint;
    public GameObject enemy;
    public int spawnCount = 1;
    public float timeBetweenSpawns = 2;
    private int _leftToSpawn;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

	}


    /// <summary>
    /// Sets up a state machine between the functions
    /// Spawner and Spawn. Spawner will call Spawn on a
    /// regular time interval as long as _leftToSpawn > 0
    /// </summary>
    public void TestWave()
    {
        _leftToSpawn = spawnCount;
        Spawner();

    }

    private void Spawner()
    {
        _leftToSpawn = _leftToSpawn - 1;
        Debug.Log("left to spawn: " + _leftToSpawn);
        Invoke("Spawn", timeBetweenSpawns);
    }

    private void Spawn()
    {
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation, tileMap.transform);
        if (_leftToSpawn > 0)
        {
            Spawner();
        }
    }



}
